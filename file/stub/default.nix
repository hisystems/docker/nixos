{ env ? { } }:

let
  nixpkgs = import <nixpkgs> { };

  name = "nixos";
  deep_merge = nixpkgs.lib.lists.foldl (
    a: b:
      if builtins.typeOf a == builtins.typeOf b then
        if builtins.isAttrs a then
          builtins.listToAttrs (map
            (name: {
              name = name;
              value = (deep_merge
                (if builtins.hasAttr name a then builtins.getAttr name a else null)
                [(if builtins.hasAttr name b then builtins.getAttr name b else null)]
              );
            })
            ((builtins.attrNames a) ++ (builtins.attrNames b))
          )
        else
          if builtins.isList a then
            a ++ b
          else
            if b == null then
              a
            else
              b
      else
        if b == null then
          a
        else
          b
  );
  environment = map
    (name: (import ("/environment/${name}")) nixpkgs)
    (builtins.attrNames
      (nixpkgs.lib.attrsets.filterAttrs (name: _: nixpkgs.lib.strings.hasSuffix ".nix" name) (builtins.readDir "/environment"))
    );
in nixpkgs.stdenv.mkDerivation (deep_merge { name = name; } (environment ++ [ env ]))
